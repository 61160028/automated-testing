const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })
  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })
  test('should 25 characters to be true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })
  test('should 26 characters to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })
  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })
  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has digit in password', () => {
    expect(checkDigit('bank26082542')).toBe(true)
  })
  test('should has 5-9 in password', () => {
    expect(checkDigit('koklim')).toBe(false)
  })
  test('should has 1-9 in password', () => {
    expect(checkDigit('koklim@bank')).toBe(false)
  })
})

describe('Test Symbol', () => {
  test('should has symbol | in password', () => {
    expect(checkSymbol('11|11')).toBe(true)
  })
  test('should has symbol @ in password', () => {
    expect(checkSymbol('@2608')).toBe(true)
  })
  test('should has symbol $ in password', () => {
    expect(checkSymbol('2608')).toBe(false)
  })
})

describe('Test Password', () => {
  test('should password bank@2542 to be true', () => {
    expect(checkPassword('bank@2542')).toBe(true)
  })
  test('should password Koklim@2542 to be true', () => {
    expect(checkPassword('Koklim@2542')).toBe(true)
  })
  test('should password bankbank25 to be false', () => {
    expect(checkPassword('bankbank25')).toBe(false)
  })
  test('should password BankBank26082542b to be false', () => {
    expect(checkPassword('BankBank26082542b')).toBe(false)
  })
})
